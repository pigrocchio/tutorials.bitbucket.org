Inspirational Quotes from Your Team in Space
--------------------------------

Branch Edit
The page is hosted under:
[http://tutorials.git.bitbucket.org](http://tutorials.git.bitbucket.org)

Images are encouraged, but should not be committed to this repo. So please
link directly to externally hosted content.

The `editme.html` is in XHTML format. This is a little stricter than HTML and
among other things requires every tag to be explicitly closed. For the
`<img />` tag that means it MUST end with `/>`.

Please don't change another user's quote as I'll reject that as well. After
all, some quotes are meaningful to the people who submit them.

**NOTE**: For some reason many editors are placing an additional space before
the start tag for the file `<!--`. This causes our parser to reject the pull request.
You can check for this error after you push your change to your repository by doing 
the following. 

1. Click **Source**. 
2. Click the `editme.html` file.
3. Click **Edit**.
4. Look for a red dot next to the start tag `<!--`.

	a. If there is a red dot, place your cursor between it and the `<!--` and press 
           backspace or delete to remove the extra space.
	   
	b. If there is no red dot you should be fine. 
	
5. Click **Commit** if you made a change or **Cancel** if you did not. 

**Bonus!** you just learned how to use the Bitbucket online editor.